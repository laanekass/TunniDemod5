package ee.bcskoolitus.finalKeyWord;

public class IfStatementsDemo {

	public static void main(String[] args) {
		boolean didItSnowYesterday = false;

		if (didItSnowYesterday) {
			System.out.println("You have to shovel snow");
		} else {
			System.out.println("Enjoy winter!");
		}
		System.out.println("print somethings");

		String instructions = (didItSnowYesterday) ? "You have to shovel snow" : "Enjoy winter!";
		System.out.println(instructions);

		// same
		if (didItSnowYesterday)
			System.out.println("You have to shovel snow");
		else
			System.out.println("Enjoy winter!");
		System.out.println("print somethings");

		int age = 18;
		if (age <= 10) {
			System.out.println("Lunch time is at 11:00");
		} else if (age > 10 && age <= 13) {
			System.out.println("Lunch time is at 12:30");
		} else if (age <= 19) {
			System.out.println("Lunch time is at 13:30");
		} else {
			System.out.println("Whenever you are ready");
		}

		String lunchTimeText = (age <= 10) ? "Lunch time is at 11:00"
				: ((age <= 13) ? "Lunch time is at 12:30"
						: ((age <= 19) ? "Lunch time is at 13:30" : "Whenever you are ready"));
		System.out.println(lunchTimeText);
	}
}
