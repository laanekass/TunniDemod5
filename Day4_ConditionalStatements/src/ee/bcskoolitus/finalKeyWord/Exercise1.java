package ee.bcskoolitus.finalKeyWord;

public class Exercise1 {

	public static void main(String[] args) {
		int givenNumber = Integer.parseInt(args[0]);

		if (givenNumber % 2 == 0) {
			System.out.println("Given number, " + givenNumber + ", is even");
		} else {
			System.out.println("Given number, " + givenNumber + ", is odd");
		}

		System.out.println((givenNumber % 2 == 0) ? "Given number, " + givenNumber + ", is even"
				: "Given number, " + givenNumber + ", is odd");
	}

}
