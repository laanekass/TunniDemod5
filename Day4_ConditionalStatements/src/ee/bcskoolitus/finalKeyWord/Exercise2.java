package ee.bcskoolitus.finalKeyWord;

public class Exercise2 {

	public static void main(String[] args) {
		String trafficLightColor = args[0];

		switch (trafficLightColor.toLowerCase()) {
		case "green":
			System.out.println("Driver can drive a car.");
			break;
		case "yellow":
			System.out.println("Driver has to be ready to stop the car or to start driving.");
			break;
		case "red":
			System.out.println("Driver has to stop car and wait for green light");
			break;
		default:
			System.out.println("Is driver feeling OK?");
		}

	}

}
