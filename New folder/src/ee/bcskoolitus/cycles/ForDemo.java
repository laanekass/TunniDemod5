package ee.bcskoolitus.cycles;

public class ForDemo {

	public static void main(String[] args) {
		int counter = 1;
		for (; counter <= 15;) {
			// counter += 3;
			System.out.println("Counter value is " + counter);
			counter += 3;
		}
		System.out.println("Counter final value is " + counter);
	}

}
