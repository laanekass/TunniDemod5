package ee.bcskoolitus.cycles;

public class WhileDemo {

	public static void main(String[] args) {
		int counter = 5;

		while (counter < 16) {
			System.out.println("counter has value " + counter);
			counter++;
		}

		int myNumber = 12;
		while (myNumber % 10 != 0) {
			System.out.println("My number is " + myNumber);
			myNumber++;
		}

		double[] childrensShotputResults1 = { 10.5, 12.2, 11.3, 10.9, 10.9 };
		int rowNo = 0;
		while (rowNo < childrensShotputResults1.length) {
			System.out.println(childrensShotputResults1[rowNo]);
			rowNo++;
		}

		String[][] peopleFromSchools = { { "Mari", "TTÜ" }, { "Jüri", "TÜ" }, { "Kalle", "TÜ" }, { "Mati", "TLÜ" } };
		int rowNo2 = 0;
		while (rowNo2 < peopleFromSchools.length) {
			int colNo = 0;
			while (colNo < peopleFromSchools[rowNo2].length) {
				System.out.print(peopleFromSchools[rowNo2][colNo] + " ");
				colNo++;
			}
			System.out.println();
			rowNo2++;
		}
	}
}
