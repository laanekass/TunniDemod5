package ee.bcskoolitus.cycles;

public class ForEachDemo {

	public static void main(String[] args) {
		double[] childrensShotputResults1 = { 10.5, 12.2, 11.3, 10.9, 10.9 };

		for (double singleResult : childrensShotputResults1) {
			System.out.println(singleResult);
		}

		String[][] peopleFromSchools = { { "Mari", "TTÜ" }, { "Jüri", "TÜ" }, { "Kalle", "TÜ" }, { "Mati", "TLÜ" } };
		for (String[] personFromSchool : peopleFromSchools) {
			for (String name : personFromSchool) {
				System.out.print(name + " ");
			}
			System.out.println();
		}

	}

}
