package ee.bcskoolitus.cycles;

public class DoWhileDemo {

	public static void main(String[] args) {
		int counter = 0;
		do {
			System.out.println("counter has value " + counter);
			counter++;
		} while (counter <= 5);

		boolean isDoorClosed = false;
		do {
			System.out.println("closing the door");
			isDoorClosed = true;
		} while (!isDoorClosed);
	}

}
