package ee.bcskoolitus.stringsdemo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ScannerDemo {

	public static void main(String[] args) {
		String scannerText = "Name: Heleen Maibak Name: Henn Sarv Name: Mari Kask";

		// default - separate by spaces
		System.out.println("============default===========");
		Scanner scannerDefault = new Scanner(scannerText);
		while (scannerDefault.hasNext()) {
			System.out.println(scannerDefault.next());
		}
		scannerDefault.close();

		System.out.println("============By given phrase===========");
		Scanner scannerPhrase = new Scanner(scannerText);
		scannerPhrase.useDelimiter("Name:");
		while (scannerPhrase.hasNext()) {
			System.out.println(scannerPhrase.next());
		}
		scannerPhrase.close();

		System.out.println("============From file by given phrase===========");
		try (Scanner scannerFromFile = new Scanner(new File("testFile.txt")).useDelimiter("Name:")) {
			while (scannerFromFile.hasNext()) {
				System.out.println(scannerFromFile.next());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
