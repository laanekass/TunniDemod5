package ee.bcskoolitus.stringsdemo;

public class StringsDemo {

	public static void main(String[] args) {
		String sentence1 = "This is one simple sentence";
		System.out.println(sentence1);

		String sentenceWithEscapingSlash = "This sentence contains \\.";
		System.out.println(sentenceWithEscapingSlash);

		String sentenceWithCitation = "\"To be or not to be\" - he said";
		System.out.println(sentenceWithCitation);

		String sentenceWithTabAndLineBreak = "part before tab \t and after tab, but before "
				+ "line break \nfinal part is after line break";
		System.out.println(sentenceWithTabAndLineBreak);

		String mySentence = "Hello!";
		String yourSentence = "Hello!";
		System.out.println("Is my sentence same as yours? " + (mySentence == yourSentence));
		System.out.println("Is my sentence same as yours? " + mySentence.equals(yourSentence));

		mySentence = "Hello!";
		yourSentence = "Hel".concat("lo!");
		System.out.println("Is my sentence same as yours? " + (mySentence == yourSentence));
		System.out.println("Is my sentence same as yours? " + mySentence.equals(yourSentence));

		String text1 = "initial statement";
		String text2 = text1;
		System.out.println("text1 = " + text1 + "\ntext2 = " + text2);

		text1 = "updated text";
		System.out.println("text1 = " + text1 + "\ntext2 = " + text2);

		String textWiythInt = "87";
		int numberFromText = Integer.parseInt(textWiythInt);
		System.out.println(numberFromText);

		double doubleFromText = Double.parseDouble(textWiythInt);
		System.out.println(doubleFromText);

		double justANumber = 2.875;
		String textFromNumber = String.valueOf(justANumber);
		System.out.println(textFromNumber);

		Double justBNumber = 2.78;
		String textFromBNumber = justBNumber.toString();
		System.out.println(textFromBNumber);

		String stringWithSpaces = "   hkkj.kdf jhfkajaaastaaskd.fj asdfa.aj.skdjf   ";
		System.out.println(stringWithSpaces);
		System.out.println(stringWithSpaces.trim());
		System.out.println(stringWithSpaces.replaceAll(" ", ""));

		System.out.println(stringWithSpaces.replaceAll(" ", ";"));
		System.out.println(stringWithSpaces.trim().replaceAll(" ", ";"));
		System.out.println(stringWithSpaces.replaceAll("j", "ai"));

		System.out.println(stringWithSpaces.replace(".", ","));
		System.out.println(stringWithSpaces.replaceAll(".", ","));
		System.out.println(stringWithSpaces.replaceAll("[.]", ","));

		System.out.println("=====================");
		System.out.println(stringWithSpaces);
		System.out.println(stringWithSpaces.replace("a.a", "!!!"));
		System.out.println(stringWithSpaces.replaceAll("[a.a]", "!!!"));
		System.out.println(stringWithSpaces.replaceAll("[a]*[a]", "!!!"));

		String template = "this is number %s";
		System.out.println(String.format(template, 1));
		System.out.println(String.format(template, 2));
		System.out.println(String.format(template, 3));
		System.out.println(String.format(template, 4));

		for (int i = 1; i <= 10; i++) {
			System.out.println(String.format(template, i));
		}
	}

}
