package ee.bcskoolitus.stringsdemo;

public class SplitDemo {

	public static void main(String[] args) {
		String textContainingListOfCityName = "This is a list of city names: Tallinn, Viljandi, Pärnu, Narva";
		String[] splittedSentece = textContainingListOfCityName.split(":");
		System.out.println(splittedSentece[0]);
		System.out.println(splittedSentece[1]);

		String textWithCityNamesOnly = splittedSentece[1];
		String[] cityNames = textWithCityNamesOnly.split(",");
		System.out.println(cityNames[0].trim());
		System.out.println(cityNames[1].trim());
		System.out.println(cityNames[2].trim());
		System.out.println(cityNames[3].trim());

		// same code again, shorter
		for (String cityName : "This is a list of city names: Tallinn, Viljandi, Pärnu, Narva"
				.split(":")[1]
				.split(",")) {
			System.out.println(cityName.trim());
		}

	}

}
