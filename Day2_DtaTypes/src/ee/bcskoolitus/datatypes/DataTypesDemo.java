package ee.bcskoolitus.datatypes;

/**
 * javadoc kommentaar
 * 
 * @author heleen
 *
 */
public class DataTypesDemo {

	public static void main(String[] args) {
		byte myFirstSmallByte = 127;
		byte mySecondSmallByte = 10;

		System.out
				.println("My first byte has value " + myFirstSmallByte + " and second has value " + mySecondSmallByte);

		System.out.println("just second has value " + mySecondSmallByte);

		byte thirdSmallByte = (byte) (myFirstSmallByte + mySecondSmallByte);
		System.out.println("third one has value " + thirdSmallByte);

		int bytesSum = myFirstSmallByte + mySecondSmallByte;
		System.out.println("bytes sum has value " + bytesSum);

		System.out.println("sum1 = " + myFirstSmallByte + mySecondSmallByte);
		System.out.println("sum2 = " + (myFirstSmallByte + mySecondSmallByte));

		Byte myFirstBigByte = 115;
		Byte mySecondBigByte = -5;

		System.out.println(myFirstBigByte + mySecondBigByte);
		System.out.println(myFirstBigByte.compareTo(mySecondBigByte));

		int bigBytesSum = myFirstBigByte + mySecondBigByte;

		boolean isDoorOpen = true;
		System.out.println("Is door open?  - " + isDoorOpen);

		char littleChar = 'a';
		System.out.println("littleChar value is " + littleChar);

		char littleChar2 = '\u0078';
		System.out.println("littleChar value is " + littleChar2);

		// char littleChar3 = 'aha';
		// System.out.println("littleChar value is " + littleChar3);

		float fluatNumber = 25.7f;
		long longNumber = 2546789653781263L;
		double doubleNumber = 25.7d;

		System.out.println("kas 1 == 1: " + (1 == 1));
		System.out.println("kas 1 == 5: " + (1 == 5));

		System.out.println("kas 1 ei ole võrdne 1: " + (1 != 1));
		System.out.println("kas 1 ei ole võrdne 5: " + (1 != 5));

		int arvA = 88;
		int arvB = 77;
		String textA = "88";
		String textB = "88";

		System.out.println("Kas arvA == arvB: " + (arvA == arvB));
		System.out.println("Kas textA == textB: " + (textA == textB)); // seda varianti ära kasuta
		System.out.println("Kas textA == textB: " + (textA.equals(textB)));
		System.out.println("Kas textA on erinev textBst: " + (!textA.equals(textB)));

		System.out.println("arvA oli " + arvA);
		arvA = 17;
		System.out.println("arvA nüüd on " + arvA);

		arvA++;
		System.out.println("arvA peale ++-i: " + arvA);

		arvA = arvA + 1;
		System.out.println("arvA peale peale ühe liitmist: " + arvA);
	}

}
