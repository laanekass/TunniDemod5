package ee.bcskoolitus.operands;

public class OperandsDemo {

	public static void main(String[] args) {
		int a = 5;
		int b = ++a;
		System.out.println("a = " + a + "; b = " + b);

		int a1 = 5;
		int b1 = a1++;
		System.out.println("a1 = " + a1 + "; b1 = " + b1);

		int a2 = 8;
		a2 += 7;
		System.out.println("a2 = " + a2);

		a2 /= 3;
		System.out.println("peale jagamist a2 = " + a2);

		boolean isDoorOpen = false;
		int outDoorTemp = -20;

		boolean isWarmInCellar = !isDoorOpen || (outDoorTemp > 0);
		System.out.println("Is warm in the cellar: " + isWarmInCellar);

		String season = "spring";
		int age = 5;

		boolean hasToGoToSchool = !season.equals("summer") && age >= 7;
		System.out.println("Child has to go to school? " + hasToGoToSchool);

		System.out.println(" -5 % 4 = " + -5 % 4);
		System.out.println(" -1 % 4 = " + -1 % 4);
		System.out.println(" 2 % 4 = " + 2 % 4);
		System.out.println(" 3 % 4 = " + 3 % 4);
		System.out.println(" 4 % 4 = " + 4 % 4);
		System.out.println(" 5 % 4 = " + 5 % 4);
		System.out.println(" 6 % 4 = " + 6 % 4);
		System.out.println(" 7 % 4 = " + 7 % 4);
		System.out.println(" 8 % 4 = " + 8 % 4);
		System.out.println(" 9 % 4 = " + 9 % 4);

		int givenNumber = 8;
		boolean isNumberEven = (givenNumber % 2) == 0;
		System.out.println("Is number " + givenNumber + " even? " + isNumberEven);

	}

}
