package ee.bcskoolitus.arrays;

public class ArraysDemo {

	public static void main(String[] args) {
		double[] childrensShotputResults1 = { 10.5, 12.2, 11.3, 10.9, 10.9 };
		for (int rowNo = 0; rowNo < childrensShotputResults1.length; rowNo++) {
			System.out.println(childrensShotputResults1[rowNo]);
		}

		double[] childrenShotputResults2 = new double[5];
		childrenShotputResults2[0] = 10.5;
		childrenShotputResults2[4] = 10.9;
		childrenShotputResults2[1] = 12.5;
		childrenShotputResults2[3] = 10.9;
		childrenShotputResults2[2] = 11.3;
		System.out.println(childrenShotputResults2[2]);

		String[][] peopleFromSchools = { { "Mari", "TTÜ" }, { "Jüri", "TÜ" }, { "Kalle", "TÜ" }, { "Mati", "TLÜ" } };
		System.out.println(peopleFromSchools[1][0] + " studied at " + peopleFromSchools[1][1]);

		String[][] peopleFromSchools2 = new String[4][2];
		peopleFromSchools2[0][0] = "Mari";
		peopleFromSchools2[0][1] = "TTÜ";
		peopleFromSchools2[1][0] = "Jüri";
		peopleFromSchools2[1][1] = "TÜ";
		peopleFromSchools2[2][0] = "Kalle";
		peopleFromSchools2[2][1] = "TÜ";
		peopleFromSchools2[3][0] = "Mati";
		peopleFromSchools2[3][1] = "TLÜ";
		System.out.println(peopleFromSchools2[1][0] + " studied at " + peopleFromSchools2[1][1]);

		String[] humanWithSchool = peopleFromSchools2[0];
		System.out.println(humanWithSchool[0] + " studied at " + humanWithSchool[1]);

		for (int rowNo = 0; rowNo < peopleFromSchools2.length; rowNo++) {
			for (int colNo = 0; colNo < peopleFromSchools2[rowNo].length; colNo++) {
				System.out.print(peopleFromSchools2[rowNo][colNo] + " ");
			}
			System.out.println();
		}

		for (int rowNo = 0; rowNo < peopleFromSchools2.length; rowNo++) {
			System.out.println(peopleFromSchools2[rowNo][0] + " studied at " 
						+ peopleFromSchools2[rowNo][1]);
		}

	}

}
