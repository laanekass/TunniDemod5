package ee.bcskoolitus.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		List<String> fruits = new ArrayList<>();
		System.out.println(fruits.size());
		fruits.add("apple");
		fruits.add("pear");
		fruits.add("ananas");
		fruits.add("apple");
		fruits.add("cherry");
		System.out.println(fruits.size());
		System.out.println(fruits.get(2));
		String blum = "blum";
		fruits.add(blum);
		System.out.println("Contains blum? " + fruits.contains(blum));

		fruits.add("banana");
		System.out.println("Contains banana? " + fruits.contains("banana"));

		fruits.remove("apple");
		System.out.println(fruits);

		fruits.set(3, "mango");
		System.out.println(fruits);

		for (String fruit : fruits) {
			System.out.println(fruit);
		}

		System.out.println("===================================");
		List<List<String>> peopleWithSchools = new ArrayList<>();
		peopleWithSchools.addAll(Arrays.asList(new ArrayList<>(Arrays.asList("Mari", "TTÜ")), 
				new ArrayList<>(Arrays.asList("Jüri", "TÜ")),
				new ArrayList<>(Arrays.asList("Kalle", "TÜ")), 
				new ArrayList<>(Arrays.asList("Mati", "TLÜ"))));		
		List<String> kati = new ArrayList<>();
		kati.add("Kati");
		kati.add("Viljandi");
		peopleWithSchools.add(kati);		
		System.out.println(peopleWithSchools.get(1).get(0));		
		List<String> jyri = peopleWithSchools.get(1);
		System.out.println(jyri);
		String name = jyri.get(0);
		System.out.println(name);		
		// mati -> matti
		peopleWithSchools.get(3).set(0, "Matti");
		System.out.println(peopleWithSchools);		
		// remove kati
		peopleWithSchools.remove(kati);		
		// add age to everyone
		for(List<String> person: peopleWithSchools) {
			person.add("35");
		}
		System.out.println(peopleWithSchools);
		
	}

}
