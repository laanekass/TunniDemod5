package ee.bcskoolitus.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
		Set<Double> childrenShotputResults = new HashSet<>();
		childrenShotputResults.add(19.6);
		childrenShotputResults.add(12.2);
		childrenShotputResults.add(11.3);
		childrenShotputResults.add(10.5);
		childrenShotputResults.add(10.9);
		
		System.out.println(childrenShotputResults.size());
		System.out.println(childrenShotputResults);
		for(Double result : childrenShotputResults) {
			System.out.println(result.hashCode());
		}
	
		Set<Double> childrenShotputResultsTree = new TreeSet<>();
		childrenShotputResultsTree.add(19.6);
		childrenShotputResultsTree.add(12.2);
		childrenShotputResultsTree.add(11.3);
		childrenShotputResultsTree.add(10.5);
		childrenShotputResultsTree.add(10.9);
		
		System.out.println(childrenShotputResultsTree);
		childrenShotputResultsTree.remove(10.5);
	}

}
