package ee.bcskoolitus.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MapDemo {

	public static void main(String[] args) {
		Map<String, List<String>> peopleWithSchools = new HashMap<>();
		peopleWithSchools.put("Mari", new ArrayList<>(Arrays.asList("Mari", "TTÜ", "30")));
		peopleWithSchools.put("Jüri", new ArrayList<>(Arrays.asList("Jüri", "TÜ", "35")));
		peopleWithSchools.put("Kalle", new ArrayList<>(Arrays.asList("Kalle", "TÜ", "42")));
		peopleWithSchools.put("Mati", new ArrayList<>(Arrays.asList("Mati", "TLÜ", "28")));
		System.out.println(peopleWithSchools);

		System.out.println(peopleWithSchools.get("Kalle").get(2));
		//print keys version 1
		System.out.println(peopleWithSchools.keySet());
		//print keys version 2
		for (String key : peopleWithSchools.keySet()) {
			System.out.println(key);
		}
		
		// print only ages
		for(List<String> person : peopleWithSchools.values()) {
			System.out.println(person.get(2));
		}
	}

}
